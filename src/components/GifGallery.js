import React from 'react';
import GifItem from './GifItem';

const GifGallery = (props) => {
    const gifItems = props.gifs.map((image) => {
        return <GifItem key={image.id}
            gif={image}
            onGifSelect={props.onGifSelect} />
    });

    return (
        <div className="gif-gallery">{gifItems}</div>
    );
};

export default GifGallery;