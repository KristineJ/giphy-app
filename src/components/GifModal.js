import React from 'react';
import Modal from 'react-modal';

Modal.setAppElement('#root');

const GifModal = (props) => {
    if (!props.selectedGif) {
        return <div></div>;
    } 
    const rating = props.selectedGif.rating;

    return (
        <Modal
            isOpen={props.modalIsOpen}
            onRequestClose={() => props.onRequestClose()}>
            <div className="gif-modal">
                <img src={props.selectedGif.images.original.url} />
                <p>This GIF is currently rated {rating.toUpperCase()}.
                    <br />
                    Do you find this appropriate?
                </p>
                <div>
                    <button className="gif-btn">Yes</button>
                    <button className="gif-btn">No</button>
                </div>
                <button className="gif-btn" onClick={() => props.onRequestClose()}>close</button>
            </div>
        </Modal>
    );
};

export default GifModal;