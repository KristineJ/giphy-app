import React from 'react';

class SearchBar extends React.Component {
    constructor() {
        super();
        this.state = { query: '' }
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.onSearch(event.target.value)
        }
    }

    onSearch(query) {
        this.setState({ query });
        this.props.onQueryChange(query);
    }

    render() {
        return (
            <div className="search">
                <input placeholder="Type your query, then press 'Enter'"
                    onKeyPress={this.handleKeyPress} />
                <button type="button" className="gif-btn" align="center"
                    onClick={ () => this.onSearch("random")}>Random GIF!</button>
            </div>
        );
    }
}

export default SearchBar;