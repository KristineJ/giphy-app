import React from 'react';
import request from 'superagent';
import './App.css';
import GifModal from './components/GifModal';
import SearchBar from './components/SearchBar';
import GifGallery from './components/GifGallery';


class App extends React.Component {
    constructor() {
        super();
        this.state = {
            gifs: [],
            selectedGif: null,
            modalIsOpen: false
        }
    }

    openModal(gif) {
        this.setState({
            modalIsOpen: true,
            selectedGif: gif
        });
    }

    closeModal() {
        this.setState({
            modalIsOpen: false,
            selectedGif: null
        });
    }

    handleQueryChange(query) {
        const url = `http://api.giphy.com/v1/gifs/search?q=${query.replace(/\s/g, '+')}&api_key=f30gJsgVA8Oa4gAztMI8Mqo3P9CssXMD`;

        request.get(url, (err, res) => {
            this.setState({ gifs: res.body.data })
        });
    }

    render() {
        return (
            <div>
                <SearchBar onQueryChange={query => this.handleQueryChange(query)} />
                <GifGallery gifs={this.state.gifs}
                    onGifSelect={selectedGif => this.openModal(selectedGif)} />
                <GifModal modalIsOpen={this.state.modalIsOpen}
                    selectedGif={this.state.selectedGif}
                    onRequestClose={() => this.closeModal()} />
            </div>
        );
    }
}

export default App;
